# frozen_string_literal: true

require 'sinatra'
require 'json'
Dir[File.join(__dir__, 'model', '*.rb')].sort.each { |file| require file }

before do
  content_type :json
end

get '/' do
  { content: 'hello' }.to_json
end

get '/hello' do
  nombre = params['nombre']
  { saludo: "hola #{nombre}" }.to_json
end

post '/add' do
  input = JSON.parse(request.body.read)
  a = input['a']
  b = input['b']
  calculator = Calculator.new
  { result: calculator.add(a.to_i, b.to_i) }.to_json
end
