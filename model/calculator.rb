# frozen_string_literal: true

class Calculator
  def add(a, b)
    a + b
  end
end
